﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : Singleton<Manager>
{
    private List<GameObject> mirrors;

    public List<GameObject> GetMirrors () {
        return mirrors;
    }

    public void AddMirror (GameObject mirror) {
        mirrors.Add(mirror);
    }
}
