﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    enum AIState {
        Idle,
        Patrolling,
        Attacking
    }


    [SerializeField] private AIState state = AIState.Idle;

    private EnemyTracking tracking;
    private Gun gun;
    
    void Start() {
        tracking = GetComponent<EnemyTracking>();
        gun = GetComponentInChildren<Gun>();
    }

    void Update() {
        UpdateState();
    }

    void UpdateState () {
        if (tracking.HasTargets()) {
            if (state != AIState.Attacking) {
                SetState(AIState.Attacking);
            }
        } else if (state == AIState.Attacking) {
            SetState(AIState.Idle);
        }
    }

    void SetState (AIState newState) {
        if (newState == AIState.Attacking) {
            gun.StartAttack();
        } else {
            gun.EndAttack();
        }

        state = newState;
    }
}
