﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Hologram : MonoBehaviour
{
    public static GameObject GenerateHologram (GameObject source, Shader hologramShader) {
        return ObjectToHologram(source, null, hologramShader);
    }

    private static GameObject ObjectToHologram (GameObject source, GameObject parent, Shader hologramShader) {
        // get data from source
        var originalMeshFilter = source.GetComponent<MeshFilter>();
        var mesh = originalMeshFilter == null ? null : originalMeshFilter.mesh;
        var originalMeshRenderer = source.GetComponent<MeshRenderer>();
        var materials = originalMeshRenderer == null ? null : originalMeshRenderer.materials;

        var position = source.transform.position;
        var rotation = source.transform.rotation;
        var scale = source.transform.localScale;
        var name = source.name + " (Hologram)";

        // set mesh and material data for new object
        var hologramObj = new GameObject(name);
        if (mesh != null) {
            hologramObj.AddComponent<MeshFilter>().mesh = mesh;
        }
        if (materials != null) {
            var meshRenderer = hologramObj.AddComponent<MeshRenderer>();
            meshRenderer.materials = materials;

            var mats = meshRenderer.materials;
            for (int i = 0; i < meshRenderer.materials.Length; i++){
                var baseColor = mats[i].GetColor("_BaseColor");
                baseColor.a = 0.5f;
                var fresnelColor = GetFresnelColor(source);
                
                mats[i] = new Material(hologramShader);
                mats[i].SetColor("_baseColor", baseColor);
                mats[i].SetColor("_fresnelColor", fresnelColor);
            }
            meshRenderer.materials = mats;
        }

        // set collider data for new object
        foreach (var col in source.GetComponents<Collider>()) {
            CopyCollider(col, hologramObj);
        }

        // set transform data for new object
        hologramObj.transform.parent = parent?.transform;
        hologramObj.transform.position = position;
        hologramObj.transform.rotation = rotation;
        hologramObj.transform.localScale = scale;

        hologramObj.tag = GetHologramTag(source);

        // recursively move down child heirarchy
        foreach (Transform child in source.transform) {
            ObjectToHologram(child.gameObject, hologramObj, hologramShader);
        }

        return hologramObj;
    }

    private static Color GetFresnelColor (GameObject source) {
        if (source.CompareTag("Player")) {
            return Color.green;
        } else {
            return Color.red;
        }
    }

    private static string GetHologramTag (GameObject source) {
        if (source.CompareTag("Player")) {
            return "Player (Hologram)";
        } else {
            return "Enemy (Hologram)";
        }
    }

    /// only copies basic properties (position/size)
    private static void CopyCollider (Collider collider, GameObject target) {
        switch (collider) {
            case BoxCollider boxCol:
                var boxCenter = boxCol.center;
                var boxSize = boxCol.size;
                var newBoxCol = target.AddComponent<BoxCollider>();
                newBoxCol.center = boxCenter;
                newBoxCol.size = boxSize;
                newBoxCol.isTrigger = true;
                break;
            case CapsuleCollider capsuleCol:
                var capsuleCenter = capsuleCol.center;
                var capsuleRadius = capsuleCol.radius;
                var newCapsuleCol = target.AddComponent<CapsuleCollider>();
                newCapsuleCol.center = capsuleCenter;
                newCapsuleCol.radius = capsuleRadius;
                newCapsuleCol.isTrigger = true;
                break;
            case SphereCollider sphereCol:
                var sphereCenter = sphereCol.center;
                var sphereRadius = sphereCol.radius;
                var newSphereRadius = target.AddComponent<SphereCollider>();
                newSphereRadius.center = sphereCenter;
                newSphereRadius.radius = sphereRadius;
                newSphereRadius.isTrigger = true;
                break;
            case MeshCollider meshCol:
                var mesh = meshCol.sharedMesh;
                var newMeshCol = target.AddComponent<MeshCollider>();
                newMeshCol.sharedMesh = mesh;
                newMeshCol.isTrigger = true;
                break;
            default:
                throw new System.NotImplementedException("Unrecognized collider type!");
        }
    }
}

