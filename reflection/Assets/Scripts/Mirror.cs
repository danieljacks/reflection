﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Mirror : MonoBehaviour
{
    public List<string> trackTags = new List<string> { "Player", "Enemy" };
    public List<GameObject> trackedObjects = new List<GameObject> ();
    public Dictionary<GameObject, GameObject> images = new Dictionary<GameObject, GameObject> ();
    public Shader hologramShader;


    void Update() {
        trackedObjects = TrackObjects(trackTags);

        var objectsToAddImages = trackedObjects.Where(obj => !images.ContainsKey(obj)).ToList();
        var objectsToDeleteImages = images.Where(entry => !trackedObjects.Contains(entry.Key)).Select(entry => entry.Key).ToList();

        foreach (var source in objectsToAddImages) {
            CreateImage(source);
        }

        foreach (var source in objectsToDeleteImages) {
            DeleteImage(source);
        }

        foreach (var entry in images) {
            UpdateImage(entry.Key);
        }
    }

    List<GameObject> TrackObjects (List<string> tags) {
        return tags.Select(tag => GameObject.FindGameObjectsWithTag(tag))
            .Aggregate((list1, list2) => list1.Concat(list2).ToArray())
            .ToList();
    }

    private void CreateImage (GameObject source) {
        var imageObj = Hologram.GenerateHologram(source, hologramShader); 

        images.Add(source, imageObj);
    }

    private void UpdateImage (GameObject source) {
        //TODO: figure out how the rotation should reflect...
        images[source].transform.position = GetImagePosition(source);
    }

    private void DeleteImage (GameObject source) {
        Destroy(images[source]);
        images.Remove(source);
    }

    private Vector3 GetImagePosition (GameObject source) {
        // convert source -> transform space
        // z = -z
        // convert transform space -> source

        var source_pos_in_transform_space = transform.InverseTransformPoint(source.transform.position);
        var image_pos_in_transform_space = new Vector3(source_pos_in_transform_space.x, source_pos_in_transform_space.y, -source_pos_in_transform_space.z);
        var image_pos = transform.TransformPoint(image_pos_in_transform_space);

        return image_pos;
    }
    
}
