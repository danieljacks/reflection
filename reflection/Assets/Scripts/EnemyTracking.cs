﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnemyTracking : MonoBehaviour
{
    class ObjectMemory {
        public GameObject obj;
        public Vector3 position;
        public float lastSeen;

        public ObjectMemory (GameObject _obj, Vector3 _position, float _lastSeen) {
            obj = _obj;
            position = _position;
            lastSeen = _lastSeen;
        }
    }

    public float trackingRange = 10.0f;
    public float rememberDuration = 3.0f;
    public Transform trackingSource;
    [SerializeField] private List<ObjectMemory> targets = new List<ObjectMemory> ();

    private float lastUpdatedTargets;

    void Start() {
        if (trackingSource == null) {
            trackingSource = transform;
        }

        lastUpdatedTargets = Time.time;
    }

    // finds new targets and deletes old targets that have not been seen for longer than threshold
    private List<ObjectMemory> FindTargets () {
        if (lastUpdatedTargets == Time.time) { // small optimization prevents updating targets multiple times in one frame
            return targets;
        }
        lastUpdatedTargets = Time.time;

        var players = GameObject.FindGameObjectsWithTag("Player");
        var playerHolograms = GameObject.FindGameObjectsWithTag("Player (Hologram)");

        var newMemories = players
            .Concat(playerHolograms)
            .Select(hologramObj => new ObjectMemory(hologramObj, hologramObj.transform.position, Time.time))
            .Where(memory => CanSee(memory.obj));

        var t = Time.time;
        var oldMemories = targets
            .Where(target => target.lastSeen < Time.time + rememberDuration) // seen recently
            .Where(target => newMemories.Where(memory => memory.obj == target.obj).Count() == 0); // not in newMemories

        return newMemories.Concat(oldMemories).ToList();
    }

    // this is separate from has targets because this should really prioritize targets that were not just shot at (currently will always target the first visible target in list)
    public Vector3 GetTarget () {
        targets = FindTargets();
        return targets[0].position;
    }

    public bool HasTargets () {
        targets = FindTargets();
        return targets.Count > 0;
    }

    private bool CanSee (GameObject obj) {
        RaycastHit hit;
        var obstacleMask = ~LayerMask.GetMask("TransparentFX");
        if (Physics.Raycast(trackingSource.position, obj.transform.position - trackingSource.position, out hit, trackingRange, obstacleMask)) {
            if (hit.transform.gameObject == obj) {
                return true;
            }
        }

        return false;
    }
}
