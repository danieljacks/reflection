﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Laser : MonoBehaviour
{
    public Vector3 direction;
    public int maxBounces = 5;
    public float maxRayDistance = 5f;
    public float lifetime = 1.0f;
    public List<Vector3> path;
    public Vector3 endRayDirection;

    void Start() {
        transform.LookAt(transform.position + direction, Vector3.up);

        StartCoroutine(DestroyRoutine());
    }

    void Update() {
        path = new List<Vector3> ();
        endRayDirection = GetPath(transform.position, direction, maxBounces, ref path);

        RenderPath(path, endRayDirection, maxRayDistance);
    }

    IEnumerator DestroyRoutine () {
        yield return new WaitForSeconds(lifetime);
        Destroy(gameObject);
    }

    void RenderPath (List<Vector3> path, Vector3 endRayDirection, float maxRayDistance) {
        var lineRenderer = GetComponent<LineRenderer>();
        var linePath = new List<Vector3>(path);

        // set end ray
        if (endRayDirection != Vector3.zero) {
            var endPoint = path.Last() + endRayDirection * maxRayDistance;
            linePath.Add(endPoint);
        }

        lineRenderer.useWorldSpace = true;
        lineRenderer.positionCount = linePath.Count;
        lineRenderer.SetPositions(linePath.ToArray());
    }

    Vector3 GetPath (Vector3 origin, Vector3 direction, int maxBounces, ref List<Vector3> hitPoints) {
        // initialize with origin
        if (hitPoints.Count == 0) {
            hitPoints.Add(origin);
        }

        RaycastHit hit;
        if (Physics.Raycast(origin, direction, out hit, Mathf.Infinity)) {
            hitPoints.Add(hit.point);

            if (hitPoints.Count > maxBounces || !hit.transform.CompareTag("Reflector")) {
                // stop the path with a point
                return Vector3.zero;
            }
            
            // get the bounce point and make the laser bounce
            var d_par = Vector3.Project(direction, hit.normal);
            var d_perp = direction - d_par;
            var bounce_direction = d_perp - d_par;

            // recursion
            return GetPath(hit.point, bounce_direction, maxBounces, ref hitPoints);
        } else {
            // stop the path with a ray to infinity
            return direction;
        }
    }
}
