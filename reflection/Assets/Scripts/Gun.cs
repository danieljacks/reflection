﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    public int clipSize = 2;
    public float attackDelay = 1.0f;
    public float reloadTime = 5.0f;
    public bool interruptBeforeReload = true;
    public bool initialReload;
    public Laser bulletPrefab;
    public Transform shootingSource;

    [SerializeField] private int bulletsRemaining;
    [SerializeField] private bool isAttacking = false;
    [SerializeField] private bool reloadRoutineRunning = false;
    private EnemyTracking tracking;

    private void Start () {
        bulletsRemaining = clipSize;
        tracking = GetComponent<EnemyTracking>();

        if (shootingSource == null) {
            shootingSource = transform;
        }
    }

    public void StartAttack () {
        isAttacking = true;
        if (reloadRoutineRunning == false) {
            StartCoroutine(ReloadRoutine());
        }
    }

    public void EndAttack () {
        isAttacking = false;
    }

    private IEnumerator ReloadRoutine () {
        reloadRoutineRunning = true;

        yield return StartCoroutine(AttackRoutine());
        yield return new WaitForSeconds(reloadTime);

        if (isAttacking) {
            yield return StartCoroutine(ReloadRoutine());
        }

        reloadRoutineRunning = false;
    }

    private IEnumerator AttackRoutine () {
        if (bulletsRemaining <= 0) {
            yield break;
        }

        if (interruptBeforeReload && isAttacking == false) {
            yield break;
        }

        CreateBullet ();
        yield return new WaitForSeconds (attackDelay);
        yield return StartCoroutine(AttackRoutine());
    }

    private void CreateBullet () {
        var target = tracking.GetTarget();
        var bullet = Instantiate(bulletPrefab, shootingSource.position, Quaternion.identity) as Laser;
        bullet.direction = target - shootingSource.position;
    }
}
